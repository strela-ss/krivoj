__author__ = 'Vladislav Strelnikov'


# http://e-maxx.ru/algo/euclid_algorithm
def evklid(a, b):
    if b == 0:
        return a
    else:
        return evklid(b, a % b)


# https://ru.wikipedia.org/wiki/%D0%91%D0%B8%D0%BD%D0%B0%D1%80%D0%BD%D1%8B%D0%B9_%D0%B0%D0%BB%D0%B3%D0%BE%D1%80%D0%B8%D1%82%D0%BC_%D0%B2%D1%8B%D1%87%D0%B8%D1%81%D0%BB%D0%B5%D0%BD%D0%B8%D1%8F_%D0%9D%D0%9E%D0%94
def binary_evklid(a, b):
    if a == 0 or b == 0:
        return max(a, b)
    if a == b:
        return a
    if a % 2 == 0 and b % 2 == 0:
        return 2*binary_evklid(a // 2, b // 2)
    if a % 2 == 0:
        return binary_evklid(a // 2, b)
    if b % 2 == 0:
        return binary_evklid(a, b // 2)
    if a > b:
        return binary_evklid((a - b) // 2, b)
    else:
        return binary_evklid(a, (b - a) // 2)


# http://e-maxx.ru/algo/export_extended_euclid_algorithm
def extend_evklid(a, b):
    if a == 0:
        x, y = 0, 1
        return b, x, y
    d, x1, y1 = extend_evklid(b % a, a)
    x = y1 - (b / a) * x1
    y = x1
    return d, x, y


if __name__ == '__main__':
    a = 21342332
    b = 12332
    print evklid(a, b)
    print binary_evklid(a, b)
    print extend_evklid(a, b)
