import copy


def algo(n, m, l):
    res = [[str(i) for i in xrange(n)]]
    temp = copy.copy(res)
    while len(temp) > 0:
        splitted = False
        t = temp.pop()
        # cycle through links
        for i in xrange(m):
            if splitted:
                break
            # choose right set
            for k in res[:]:
                in_list = []
                out_list = []
                for j in t:
                    try:
                        if l[(j, str(i))] in k:
                            in_list += [j]
                        else:
                            out_list += [j]
                    except KeyError:
                        out_list += [j]
                if len(in_list) > 0 and len(out_list) > 0:
                    temp += [in_list, out_list]
                    res.remove(k)
                    res += [in_list, out_list]
                    splitted = True
                    break
    return res


if __name__ == '__main__':
    # # n - num states
    # n = input('Input n: ')
    # # m - num links
    # m = input('Input m: ')
    # # links
    # l = {}
    # for i in xrange(m):
    #     f = input('from: ')
    #     th = input('through: ')
    #     t = input('to: ')
    #     l[(f, th)] = t
    n = 4
    m = 4
    l = {
        ('0', '0'): '1',
        ('0', '1'): '2',
        ('1', '0'): '3',
        ('2', '0'): '3',
    }
    print algo(n, m, l)
