__author__ = 'Vladislav Strelnikov'


# http://e-maxx.ru/algo/prefix_function
def kmp(s):
    n = len(s)
    pi = [0 for i in range(n)]
    for i in range(1, n):
        j = pi[i - 1]
        while j > 0 and s[i] != s[j]:
            j = pi[j-1]
        if s[i] == s[j]:
            j += 1
        pi[i] = j
    return pi


# http://e-maxx.ru/algo/aho_corasick
def aho(s):
    pass


if __name__ == '__main__':
    print kmp("abcabcd")
