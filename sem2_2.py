space = '-0-'
i = 0
final = set()


def next_space_one():
    global space
    space = "-%s-" % str(int(space[1:-1]) + 1)
    # space = chr(ord(space) + 1)
    return space


def next_space(n=1):
    if n == 1:
        return next_space_one()
    return [next_space_one() for _ in xrange(n)]


def get_first(x):
    return x[0][0]


def get_last(x):
    return x[-1][1]


def get_first_last(x):
    return x[0][1]


def get_last_first(x):
    return x[-1][0]


def add_final(x):
    global final
    final.add(x)


def build_primitive(data):
    global i
    if data[i] == '{':
        i += 1
        result = __iter(build(data))
        i += 1
    elif data[i] == '(':
        i += 1
        result = build(data)
        i += 1
    elif data[i] == '[':
        i += 1
        result = __omega(build(data))
        i += 1
    else:  # data[i].isalpha():
        result = single(data[i])
        i += 1
    return result


def build_mult(data):
    global i
    result = build_primitive(data)
    while i < len(data) and data[i] == '*':
        i += 1
        result = __mult(result, build_mult(data))
    return result


def build(data):
    global i
    result = build_mult(data)
    while i < len(data) and data[i] == '+':
        i += 1
        result = __sum(result, build_mult(data))
    return result


def __iter(x):
    # {x}
    # A -> B
    # B -> x
    # x -> C
    # B -> C
    # C -> B
    # C -> D
    A, B = next_space(2)
    first = get_first(x)
    last = get_last(x)
    return [[A, first]] + x + [[first, last], [last, first], [last, B]]


def __omega(x):
    def find_final():
        visited = [result[-1]]
        stack = [result[-1]]
        while len(stack) > 0:
            for i in result:
                if i not in visited and i[1] == stack[0][0]:
                    if i[0].islower():
                        add_final(tuple(i))
                        continue
                    stack.append(i)
                    visited.append(i)
            del stack[0]

    # {x}
    # A -> B
    # B -> x
    # x -> C
    # B -> C
    # C -> B
    # C -> D
    A, B = next_space(2)
    first = get_first(x)
    last = get_last(x)
    result = [[A, first]] + x + [[first, last], [last, first], [last, B]]
    find_final()
    return result


def __mult(x, y):
    # x*y
    # A -> x
    # x -> B
    # B -> y
    # y -> C
    B = next_space()
    return x[:-1] + [[get_last_first(x), B], [B, get_first_last(y)]] + y[1:]


def __sum(x, y):
    # x+y
    # A -> B
    # B -> x
    # B -> y
    # x -> C
    # y -> C
    # C -> D
    A = get_first(x)
    B = get_last(x)
    C = get_first(y)
    D = get_last(y)
    return [[A, B], [B, get_first_last(x)]] + x[1:-1] + [[get_last_first(x), C], [B, get_first_last(y)]] + \
           y[1:-1] + [[get_last_first(y), C], [C, D]]


def single(x):
    # x
    # A -> x
    # x -> B
    return [[next_space(), x], [x, next_space()]]


def numerate(data):
    num = 1
    mask = {
        0: '',
    }
    global final
    for i, e in enumerate(data):
        if isinstance(e[1], str) and e[1].islower():
            if tuple(data[i + 1]) in final:
                final.remove(tuple(data[i + 1]))
                add_final(num)
            mask[num] = e[1]
            e[1] = num
            data[i + 1][0] = num
            num += 1
    return data, mask


def find_numbers(data, start, visited):
    visited += [start]
    res = []
    for i in data:
        if i[0] == start and isinstance(i[1], int):
            res += [i[1]]
        if i[1] in visited:
            continue
        if i[0] == start and isinstance(i[1], str):
            c = find_numbers(data, i[1], visited)
            if c and isinstance(c, int):
                res += [c]
            elif c and isinstance(c, list):
                res += c
    return res


def transform(data, mask):
    result = []
    for i in mask.keys():
        for k in data:
            if k[0] == i:
                destination = find_numbers(data, i, [])
                for d in destination:
                    result += [[i, d, mask[d]]]
    return result


def determination(data):
    queue = [[0]]
    result = []
    while len(queue) > 0:
        # print result
        used = []
        for j in queue[0]:
            for i in data:
                if i[0] == j and i[2] not in used:
                    used.append(i[2])
                    group = set()
                    for p in queue[0]:
                        for k in data:
                            if k[0] == p and k[2] == i[2]:
                                group.add(k[1])
                    group = sorted(list(group))
                    if [queue[0], group, i[2]] in result:
                        continue
                    result.append([sorted(queue[0]), sorted(group), i[2]])
                    if set(queue[0]) != set(group) and group not in queue:
                        queue.append(group)
        del queue[0]
    return result


def find_finals(data):
    result = set()
    for i in data:
        if len(final.intersection(set(i[1]))) > 0:
            result.add(tuple(i[1]))
    return list(result)


def beautifier(data):
    num = 0
    global final

    def new_style(num):
        style = 'e' + str(num)
        num += 1
        return style, num

    for i in data:
        if isinstance(i[0], list):
            old = i[0]
            new, num = new_style(num)
            for k in data:
                if k[0] == old:
                    k[0] = new
                if k[1] == old:
                    k[1] = new
            if tuple(old) in final:
                final += [new]
                final.remove(tuple(old))
    return data


if __name__ == '__main__':
    # r = '[x+y]*x+{x}*{y}'
    r = '{a+b+c}*[b+c]+{a+b+c}*[b+(a+c)*{a+b+c}*b]'
    # r = 'x*[y]'
    from pprint import pprint

    step_0 = build(r)
    print "Final: ", final
    print "Final connectors: %s" % final
    step_1 = [[next_space(), 0], [0, get_first(step_0)]] + step_0
    print "\nSTEP 1"
    pprint(step_1)
    step_2, step_3 = numerate(step_1)
    print "Final: ", final
    print "\nSTEP 2"
    pprint(step_2)
    print "\nSTEP 3"
    print "Finals: ", list(final)
    print "Numbers: ", step_3.keys()
    step_4 = transform(step_2, step_3)
    print "\nSTEP 4"
    pprint(step_4)
    step_5 = determination(step_4)
    print "\nSTEP 5"
    pprint(step_5)
    final = find_finals(step_5)
    print "Finals: ", final
    print '\nSTEP 6'
    step_6 = beautifier(step_5)
    pprint(step_6)
    print "Finals: ", final
