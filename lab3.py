from random import random
import timeit

__author__ = 'Vladislav Strelnikov'

BIG_POLY = [int(random() * 10) for i in range(1000)]
SMALL_POLY = [int(random() * 10) for i in range(10)]
CUSTOM_POLY = [1, 1, 1, 1]
poly = BIG_POLY
x = 5


def simple():
    return sum([a * x ** i for i, a in enumerate(poly)])


def gorner_recurcive(i=0):
    if i == len(poly) - 1:
        return poly[i]
    return poly[i] + x * gorner_recurcive(i + 1)


def gorner():
    res = 0
    for i in range(len(poly) - 1, -1, -1):
        res *= x
        res += poly[i]
    return res


def timer(fun, **kwargs):
    time = timeit.timeit(lambda: fun(**kwargs), number=100)
    print "Method: %s\nResult: %s\nTime: %s\n%s" % \
          (fun.__name__, fun(**kwargs), time, "".join(["-" for i in xrange(40)]))


if __name__ == '__main__':
    timer(simple)
    # timer(gorner_recurcive)
    timer(gorner)
    # print gorner(poly, x)
