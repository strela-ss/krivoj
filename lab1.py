__author__ = 'Vladislav Strelnikov'


# http://e-maxx.ru/algo/big_integer
def print_long(data):
    print "".join(str(i) for i in data)


def myreverse(a):
    return list(reversed(a))


def classic_sum(a, b, base=10):
    a, b = myreverse(a), myreverse(b)
    i, carry = 0, 0
    while i < max(len(a), len(b)) or carry:
        if i == len(a):
            a += [0]
        if i < len(b):
            a[i] += carry + b[i]
        else:
            a[i] += carry
        carry = a[i] >= base
        if carry:
            a[i] -= base
        i += 1
    return myreverse(a)


def classic_diff(a, b, base=10):
    a, b = myreverse(a), myreverse(b)
    i, carry = 0, 0
    while i < len(b) or carry:
        if i < len(b):
            a[i] -= carry + b[i]
        else:
            a[i] -= carry
        carry = a[i] < 0
        if carry:
            a[i] += base
        i += 1
    while len(a) > 1 and a[-1] == 0:
        a.pop()
    return myreverse(a)


def classic_mult(a, b, base=10):
    a, b = myreverse(a), myreverse(b)
    c = [0 for i in xrange(100)]
    i = 0
    while i < len(a):
        j, carry = 0, 0
        while j < len(b) or carry:
            if j < len(b):
                current = c[i + j] + a[i] * b[j] + carry
            else:
                current = c[i + j] + carry
            c[i + j] = current % base
            carry = current // base
            j += 1
        i += 1
    while len(c) > 1 and c[-1] == 0:
        c.pop()
    return myreverse(c)


def classic_div(a, b, base=10):
    a = myreverse(a)
    i, carry = len(a) - 1, 0
    while i >= 0:
        current = a[i] + carry * base
        a[i] = current // b
        carry = current % b
        i -= 1
    while len(a) > 0 and a[-1] == 0:
        a.pop()
    return myreverse(a)


if __name__ == '__main__':
    res = classic_sum([9, 0, 9], [9, 8, 7, 9])
    print_long(res)
    res = classic_diff([9, 0, 9], [9, 7])
    print_long(res)
    res = classic_mult([9, 0, 8], [3, 5])
    print_long(res)
    res = classic_div([1, 5, 3, 2, 8], 24)
    print_long(res)
